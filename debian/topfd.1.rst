=======
 topfd
=======

---------------------------------------------
Top-down mass spectral Feature Detection
---------------------------------------------

:Author: Filippo Rusconi <lopippo@debian.org> and upstream authors (Dr. Xiaowen
         Liu's Lab at Indiana University-Purdue University Indianapolis and
         others)
:Date: 20200521  
:Copyright: Filippo Rusconi and Indiana University-Purdue University
            Indianapolis
:Version: 1
:Manual section: 1
:Manual group: TopPIC suite


SYNOPSIS
========

  topfd [options] spectrum-file-names


DESCRIPTION
===========

TopFD (Top-down mass spectral Feature Detection) is a software tool for top-down
spectral deconvolution, which groups top-down mass spectral peaks into
isotopomer envelopes and converts isotopomer envelopes to monoisotopic neutral
masses. In addition, it extracts proteoform features from MS1 spectra.

1. Input

   The input of TopFD is mass spectrometry data files in the mzXML or mzML
   format. Raw mass spectral data generated from various mass spectrometers can
   be converted to mzML or mzXML files using msconvert.

2. Output

   TopFD outputs two LC/MS feature text files with a file extension "feature",
   one LC/MS feature file with a file extension "xml", and a deconvoluted mass
   spectral data file for MS/MS spectra in the msalign format with a file
   extension "msalign", which is similar to the MGF file format. In addition,
   TopFD creates two folders containing java scripts files of spectral data for
   spectral visualization and other files generated in spectral deconvolution.
   For example, when the input file name is spectra.mzML, the output includes:

    * spectra_ms1.feature: a feature file containing LC/MS features.
    * spectra_ms2.feature: a feature file containing MS/MS scan IDs and their corresponding LC/MS feature IDs.
    * spectra_feature.xml: a feature file containing LC/MS features in the xml format.
    * spectra_ms2.msalign: a list of deconvoluted MS/MS spectra.
    * spectra_file: a folder containing deconvoluted MS1 spectra and annotations of LC/MS data.
    * spectra_html: a folder containing java script files of MS1 and MS/MS data for spectral visualization.


OPTIONS
=======

-h [ --help ] Print the help message.

-c [ --max-charge ] <a positive integer> Set the maximum charge state of precursor and fragment ions. The default value is 30.

-m [ --max-mass ] <a positive number> Set the maximum monoisotopic mass of precursor and fragment ions. The default value is 100,000 Dalton.

-e [ --mz-error ] <a positive number> Set the error tolerance of m/z values of spectral peaks. The default value is 0.02 m/z.

-r [ --ms-one-sn-ratio ] <a positive number> Set the signal/noise ratio for MS1 spectra. The default value is 3.

-t [ --ms-two-sn-ratio ] <a positive number> Set the signal/noise ratio for MS/MS spectra. The default value is 1.

-w [ --precursor-window ] <a positive number> Set the precursor isolation window size. The default value is 3.0 m/z.

-o [ --missing-level-one ] Specify that the input file does not contain MS1 spectra.


EXAMPLES
========

* Deconvolute a centroid data file spectra.mzML and output four files: spectra_ms2.msalign, spectra_ms1.feature, spectra_ms2.feature, spectra_feature.xml.

  topfd spectra.mzML

* Deconvolute two centroid data files spectra1.mzML and spectra2.mzML and output three files for each input data file.

  topfd spectra1.mzML spectra2.mzML

* Deconvolute all centroid data files in the current folder.

  topfd \*.mzML

* Deconvolute a centroid data file spectra.mzML that does not contain MS1 spectra.

  topfd -o spectra.mzML

* Deconvolute a centroid data file spectra.mzML with a precursor isolation window size 2 m/z.

  topfd -w 2 spectra.mzML

* Deconvolute a centroid data file spectra.mzML with a signal/noise ratio 2 for MS1 spectra.

  topfd -r 2 spectra.mzML

* Deconvolute a centroid data file spectra.mzML with the following settings: the maximum charge state: 50, the maximum mass: 30,000 Dalton, and the signal/noise ratio for MS/MS spectra: 2.

  topfd -c 50 -m 30000 -t 2 spectra.mzML 


SEE ALSO
========

* toppic (1)
* topmg (1)
* topdiff (1)


MAN PAGE PRODUCTION
===================

This man page was written by Filippo Rusconi <lopippo@debian.org>. Material was
taken from http://proteomics.informatics.iupui.edu/software/toppic/manual.html.
